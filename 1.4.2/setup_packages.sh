#!/bin/sh

KOMPIRA_REPO_DIR=/opt/kompira/repo
MOUNT_DEVICE=/dev/dvd
SETUP_LOG=pkg_setup.$$.log
THIS_DIR=`dirname \`readlink -f $0\``

start_setup()
{
    echo "***"
    echo "*** Kompira に必要な外部パッケージをインストールします"
    echo "***"
}

exit_setup()
{
    echo "***"
    echo "*** 外部パッケージのインストールが完了しました"
    echo "*** (引き続き Kompira のインストールを行ってください)"
    echo "***"
}

exit_if_failed()
{
    local status message
    status="$1"
    message="$2"
    if [ "$status" -ne 0 ]
    then
        echo
        echo -e "*** [ERROR] ${message}"
        echo
        exit 1
    fi
}

mount_dvd()
{
    echo    
    echo "*** DVD ドライブをマウントします"
    echo
    mkdir -p /media/CentOS
    mount -t iso9660 $MOUNT_DEVICE /media/CentOS
    exit_if_failed "$?" "DVD ドライブのマウントに失敗しました\n(CentOS インストール DVD がセットされていることを確認してください)"
}

copy_kompira_repo()
{
    echo
    echo "*** Kompira 用レポジトリをコピーします"
    echo
    mkdir -p /opt/kompira
    cp -r $THIS_DIR/repo $KOMPIRA_REPO_DIR
    exit_if_failed "$?" "Kompira 用レポジトリのコピーに失敗しました"
}

install_utils()
{
    echo
    echo "*** セットアップ用パッケージをインストールします"
    echo
    local pkg
    for pkg in yum-utils python-setuptools
    do 
	yum --disablerepo=* --enablerepo=c6-media install -y $pkg
	exit_if_failed "$?" "$pkg のインストールに失敗しました"
    done

}

add_repository()
{
    echo
    echo "*** 拡張レポジトリを追加します"
    echo
    local pkg
    local repo
    for pkg in epel-release-6-8.noarch.rpm pgdg-redhat93-9.3-1.noarch.rpm
    do
	rpm -i $THIS_DIR/$pkg
	exit_if_failed "$?" "$pkg のインストールに失敗しました"
    done
    for repo in Openwsman.repo Kompira.repo
    do
	yum-config-manager --disablerepo=* --enablerepo=c6-media --add-repo=$THIS_DIR/$repo
	exit_if_failed "$?" "レポジトリ $repo の追加に失敗しました"
    done
}

config_repository()
{
    echo
    echo "*** レポジトリの設定を行います"
    echo
    local repo
    for repo in base extras updates epel pgdg93 Openwsman
    do
        yum-config-manager --disable $repo
        exit_if_failed "$?" "$repo の無効化に失敗しました"
    done
    for repo in c6-media
    do
        yum-config-manager --enable $repo
        exit_if_failed "$?" "$repo の有効化に失敗しました"
    done
}

install_yum_pkgs()
{
    echo
    echo "*** yum パッケージをインストールします"
    echo
    local pkg
    for pkg in rabbitmq-server postgresql93-server postgresql93-contrib postgresql93-devel openwsman-python
    do 
	yum install -y $pkg
	exit_if_failed "$?" "$pkg のインストールに失敗しました"
    done
}

install_pip()
{
    echo
    echo "*** pip をインストールします"
    echo
    python $THIS_DIR/pip/get-pip.py --no-index --find-links=$THIS_DIR/pip
    exit_if_failed "$?" "pip のインストールに失敗しました"
}

install_python_pkgs()
{
    echo
    echo "*** Python パッケージをインストールします"
    echo
    pip install --use-wheel --no-index --no-deps --find-links=$THIS_DIR/wheelhouse/ $THIS_DIR/wheelhouse/*
    exit_if_failed "$?" "Python パッケージのインストールに失敗しました"
}

uninstall_old_setuptools()
{
    echo
    echo "*** 古い setuptools をアンインストールします"
    echo
    pip uninstall -y setuptools
    exit_if_failed "$?" "setuptools のアンインストールに失敗しました"
}

################################################################

{
start_setup
mount_dvd
copy_kompira_repo
install_utils
add_repository
config_repository
install_yum_pkgs
install_pip
install_python_pkgs
uninstall_old_setuptools
exit_setup
} 2>&1 | tee $SETUP_LOG
