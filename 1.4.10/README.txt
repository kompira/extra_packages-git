================================================================
                        お読みください
================================================================

このアーカイブは、Kompira のインストールに必要な外部パッケージを集めた
ものです。非インターネット接続環境にあるサーバに Kompira をインストール
する場合、このアーカイブに含まれるパッケージをインストールしてから、
Kompira のインストールを行ってください。

なお、本アーカイブには、CentOS のパッケージは含まれておりませんので、別
途、CentOS のインストール DVD が必要となります。

- 対象 OS:
  CentOS 6.x 64bit版
- 対象 Kompira バージョン:
  1.4.10


パッケージインストール方法
==========================

1. CentOS のインストール DVD を インストール先サーバの DVD ドライブにセッ
   トします。

2. root 権限で本アーカイブ内の setup_package.sh (※)を実行します。 

3. CentOS のインストール DVD をセットした状態のまま、Kompira パッケージ
   内 の install.sh を実行します。

[注意]
   install.sh の以下の行を修正する必要があります。

314行目:
- DESCRIPTION="RabbitMQ 関連のパッケージ" YUM_OPTION="--enablerepo=epel" \
+ DESCRIPTION="RabbitMQ 関連のパッケージ" \

329行目:
- DESCRIPTION="PostgreSQL 関連のパッケージ" YUM_OPTION="--enablerepo=pgdg93" \
+ DESCRIPTION="PostgreSQL 関連のパッケージ" \

3323232行目:
- DESCRIPTION="追加のパッケージ" YUM_OPTION="--enablerepo=epel" \
+ DESCRIPTION="追加のパッケージ" \

---
※ このスクリプトは以下の処理を行います。

   スクリプトの実行に失敗する場合は、手動で以下のコマンドを実行してみて
   ください。

   (1) DVD ドライブのマウント
   # mkdir /media/CentOS
   # mount -t iso9660 /dev/dvd /media/CentOS

   (2) Kompira レポジトリのコピー
   # mkdir /opt/kompira
   # cp -r ./repo /opt/kompira/

   (3) yum-utils, python-setuptools をインストール
   # yum --disablerepo=* --enablerepo=c6-media install -y yum-utils python-setuptools

   (4) レポジトリ追加
   # rpm -ivh epel-release-6-8.noarch.rpm
   # rpm -ivh pgdg-redhat93-9.3-2.noarch.rpm
   # yum-config-manager --disablerepo=* --enablerepo=c6-media --add-repo=Kompira.repo

   (5) レポジトリ設定
   # yum-config-manager --disable base
   # yum-config-manager --disable extras
   # yum-config-manager --disable updates
   # yum-config-manager --disable epel
   # yum-config-manager --disable pgdg93
   # yum-config-manager --enable c6-media

   (6) yum パッケージのインストール
   # yum install -y rabbitmq-server postgresql93-server postgresql93-contrib postgresql93-devel

   (7) pip のインストール
   # python ./pip/get-pip.py --no-index --find-links=./pip --no-wheel

   (8) wheel のインストール
   # pip install wheel --find-links=wheelhouse/ --no-index --no-deps

   (9) Python パッケージのインストール
   # pip install --use-wheel --no-index --no-deps --find-links=wheelhouse/ wheelhouse/*
